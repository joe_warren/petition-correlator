function get_constituencies_data(json){
  return json.data.attributes.signatures_by_constituency
}
function get_name(json){
  return json.data.attributes.action
}
function constituencies_to_map(list){
  return _.object(_.pluck(list, "ons_code"),_.pluck(list, "signature_count")) 
}
function strip_data(list){
  return _.map(list, function(d){
    return {
      "name": d.name,
      "ons_code": d.ons_code,
      "mp": d.mp
    }
  })
}

function to_single_list(constituencies, const1, const2){
  var cmap1 = constituencies_to_map(const1)
  var cmap2 = constituencies_to_map(const2)
  return _.map(constituencies, function(d){ 
    d.d1 = 0;
    if( cmap1[d.ons_code] !== undefined ){
      d.d1 = cmap1[d.ons_code];
    }
    d.d2 = 0;
    if( cmap2[d.ons_code] !== undefined ){
      d.d2 = cmap2[d.ons_code];
    }
    return d;
  })
}

function regress(vec){
  var x = _.pluck(vec, 0);
  var y = _.pluck(vec, 1);
  var add = function(a, b){return a+b};
  var multiply = function(a){return a[0]*a[1]};
  var square = function(a){return a*a};
  
  var xBar = _.reduce(x, add)/x.length;
  var yBar = _.reduce(y, add)/y.length;
  var xNorm = _.map(x, function(a){return a-xBar});
  var yNorm = _.map(y, function(a){return a-yBar});

  var sxx = _.reduce(_.map(xNorm, square), add);
  var syy = _.reduce(_.map(yNorm, square), add);
  var sxy = _.reduce(_.map(_.zip(xNorm, yNorm), multiply), add);
  
  var slope = sxy/sxx;
  var intercept = yBar - (xBar*slope)
  var rSquared = sxy*sxy/(sxx*syy)
  return [slope, intercept, rSquared]
}

function linePoints(leastSquares, xmax, ymax){
  var slope = leastSquares[0];
  var intercept = leastSquares[1];

  var xIntercept = -intercept/slope;
  var topIntercept = (ymax - intercept)/slope;
  var xPoints = [0, xIntercept, topIntercept, xmax].sort(function(a, b){return a-b})
  var x1 = xPoints[1];
  var x2 = xPoints[2];
  var y1 = slope*x1 + intercept;
  var y2 = slope*x2 + intercept;
  return [[x1,y1], [x2, y2]]

}

var PARTY_COLOUR = {
  "UK Independence Party": "#F0F",
  "Conservative":  "#00F",
  "Scottish National Party": "#FB0",
  "Liberal Democrat":	"#FF0",
  "Labour": "#F00",
  "Labour/Co-operative": "#B00",
  "Green Party": "#0F0"
}

function plot(data, xname, yname){
  var normalizedFn = function(d){
     return [d.d1/d.population, d.d2/d.population];
  };
  var totalFn = function(d){
     return [d.d1, d.d2];
  };

  var dataFn = normalizedFn;

  var normData = _.map(data, normalizedFn);
  var totalData = _.map(data, totalFn);

  var xmaxNorm = _.max(_.map(normData, function(d){return d[0]}));
  var ymaxNorm = _.max(_.map(normData, function(d){return d[1]}));
  var xmaxTotal = _.max(_.map(totalData, function(d){return d[0]}));
  var ymaxTotal = _.max(_.map(totalData, function(d){return d[1]}));

  var xmax = xmaxNorm;
  var ymax = ymaxNorm;

  var xscale = d3.scaleLinear().domain([0, xmax]).range([0,600]);
  var yscale = d3.scaleLinear().domain([0, ymax]).range([600, 0]);

  var svg = d3.select("#svg")
  svg.attr("style", "");
  var defs = svg.insert("svg:defs");
  var canvas = svg.insert("g")
    .attr("transform", "translate(100, 100)");

  var filter = defs.append("filter")
    .attr("id", "drop-shadow")
    .attr("height", "600%");

  filter.append("feColorMatrix")
    .attr("in", "SourceAlpha")
    .attr("type", "matrix")
    .attr("values", "-1 0 0 0 1, 0 -1 0 0 1, 0 0 -1 0 1, 0 0 0 1 0")
    .attr("result", "matrix");

  filter.append("feMorphology")
    .attr("in", "matrix")
    .attr("operator", "dilate")
    .attr("radius", "1")
    .attr("result", "blur");

  var feMerge = filter.append("feMerge");

  feMerge.append("feMergeNode")
    .attr("in", "blur")
  feMerge.append("feMergeNode")
      .attr("in", "SourceGraphic");

  var mercatorPath = d3.geoPath().projection(
    d3.geoMercator()
      .scale(2000)
      .translate([400,2600]) 
  );
  var identityPath = d3.geoPath().projection(d3.geoIdentity().scale(3));

  var setHoverText = function(d){
     hoverfields.name.text(d.name);
     hoverfields.mp.text(d.mp);
     hoverfields.party.text(d.party);
     hoverfields.population.text("" + d.population + " electors");
     hoverfields.votes.text("" + d.d1 + " : " + d.d2 + " votes");
  }

  var onHoverGraph = function(d){
     setHoverText(d);
     text
        .attr("transform",
          "translate(" + 
          Math.floor(xscale(dataFn(d)[0]) + 10) + 
          ", " + 
          Math.floor(yscale(dataFn(d)[1])) + 
         ")")
        .attr("opacity", 1);
  }
  var onHoverMap = function(d){
     setHoverText(d);
     var c = mercatorPath.centroid(d.boundary);
     text
        .attr("transform",
          "translate(" + 
          Math.floor((c[0]) + 10) + 
          ", " + 
          Math.floor(c[1]) + 
         ")")
        .attr("opacity", 1);
  }

  var onLeave = function(d){
     text.attr("opacity", 0)
  }
  data = data.filter(function(d){return d.circle});

  data = data.sort(function(a, b){
    ca = mercatorPath.centroid(a.boundary);
    cb = mercatorPath.centroid(b.boundary);
    return cb[1] - ca[1];
  });

  var positionGraph = function(path){
     path.attr("transform", function(d){
      return "translate(" + xscale(dataFn(d)[0]) +", "+ yscale(dataFn(d)[1]) + ")";
    })
  };

  var applyGraphProperties = function(path){
    path
      .attr("fill-opacity", 0.6)
      .attr("stroke", "black")
      .attr("stroke-width", "0.5px")
      .attr("fill", function(d){
         if(PARTY_COLOUR[d.party]){
           return PARTY_COLOUR[d.party];
         } else {
           return "#AAA";
         }
      })
      .attr("d", function(d){ return identityPath(d.circle) });
      positionGraph(path);
  };


  var mapFn = function(d){
    if (d.d2 === 0 && d.d1 === 0){
       return 0;
    }
    return (d.d1-d.d2)/(d.d1+d.d2);
  };

  var applyMapProperties = function(path){
    path
      .attr("stroke-width", "0")
      .attr("fill-opacity", 1)
      .attr("transform", function(d){
        return "translate(0, 0)";
      })
      .attr("d", function(d){ return mercatorPath(d.boundary) })
      .attr("fill", function(d){
        return colourscale(mapFn(d));
      });
  };

  var dots = canvas.insert("g").selectAll(".dot")
    .data(data)
    .enter()
      .insert("path");
  applyGraphProperties(dots);
  dots.on("mouseover", onHoverGraph);
  dots.on("mouseout", onLeave);
  
  var graphElems = canvas.insert("g")
    .attr("opacity", 1);

  var yaxis = d3.axisLeft(yscale);
  var xaxis = d3.axisBottom(xscale);
  graphElems.insert("g")
   .attr("transform", "translate(0, 600)")
   .attr("class", "xaxis") 
   .call(xaxis)
   .append("text")
   .attr("x", 300).attr("y", 75)
   .attr("text-anchor", "middle")
   .attr("class", "label")
   .text(xname);
  graphElems.insert("g")
   .attr("class", "yaxis") 
   .call(yaxis)
   .append("text")
   .attr("class", "label")
   .attr("x", -300).attr("y", -75)
   .attr("text-anchor","middle")
   .attr("transform", "rotate(-90)")
   .text(yname);


   var rSquaredLine = graphElems.insert("line")
      .attr("class", "rsquaredLine");


   var rSquaredText = graphElems.insert("text")
     .attr("class", "rsquared");

   var positionRSquared = function(rsl, rst){
     var leastSquares = regress(_.map(data, dataFn));
     var line = linePoints(leastSquares, xscale.invert(600), yscale.invert(0));
     rsl
       .attr("x1", xscale(line[0][0]))
       .attr("y1", yscale(line[0][1]))
       .attr("x2", xscale(line[1][0]))
       .attr("y2", yscale(line[1][1]));
     rst 
       .attr("x", 600 + 5)
       .attr("y", yscale(line[1][1]))
       .attr("dy", "0.25em")
       .text(" r squared = " + leastSquares[2].toFixed(4));
   };
   positionRSquared(rSquaredLine, rSquaredText);
   var proportionButton = graphElems.insert("g")
      .attr("class", "button");
        
   proportionButton.insert("rect")
     .attr("x", 100)
     .attr("y", -45)
     .attr("width", 15)
     .attr("height", 15);

   var proportionButtonPath = proportionButton.insert("path")
     .attr("transform", "translate(95, -1077)")
     .attr("d", POPULATION_D);

   var proportionButtonText = proportionButton.insert("text")
     .attr("y", -50+25/2+5)
     .attr("x", 121)
     .attr("class", "label")
     .text("show total votes");


  var updateAxis = function(){
    xscale.domain([0, xmax]);
    yscale.domain([0, ymax]);
    xaxis.scale(xscale);
    yaxis.scale(yscale);
    d3.select(".xaxis").transition().duration(500).call(xaxis);
    d3.select(".yaxis").transition().duration(500).call(yaxis);
  };

  var updateGraphView = function(){
    updateAxis();
    positionGraph(dots.transition().delay(750).duration(1000));
    positionRSquared(
      rSquaredLine.transition().delay(750).duration(1000), 
      rSquaredText.transition().delay(750).duration(1000)
    );
  };

  var toPercentageView = function(){
    dataFn = normalizedFn;
    xmax = xmaxNorm;
    ymax = ymaxNorm;    
    updateGraphView();
  };

  var toPopulationView = function(){
    dataFn = totalFn;
    xmax = xmaxTotal;
    ymax = ymaxTotal;
    updateGraphView();
  };

   var literalPopulation = false;

   proportionButton.on("click", function(){
     var buttonTransition = proportionButtonPath.transition().duration(1000);
     literalPopulation = !literalPopulation;
     if(literalPopulation){
       buttonTransition.attr("d", PERCENTAGE_D);
       proportionButtonText.text("show percentage of electorate")
       toPopulationView();
     } else {
       buttonTransition.attr("d", POPULATION_D);
       proportionButtonText.text("show total votes")
       toPercentageView();
     }
   });

   var ratios = data.map(mapFn);
   var minRatio = d3.min(ratios);
   var maxRatio = d3.max(ratios);
   var colourscale = d3.scaleLinear()
     .domain([minRatio, maxRatio])
     .range(["#FF8800", "#8800FF"])

   var mapElems = canvas.insert("g")
       .attr("opacity", 0);

   var colourAxis = d3.axisRight( 
       d3.scaleLinear() 
         .domain([minRatio, maxRatio])
         .range([0, 600])
       );

   var colourAxisElem = mapElems.insert("g")
       .attr("transform", "translate(635, 0)")
       .call(colourAxis);
   colourAxisElem.append("text")
         .attr("x", 300).attr("y", -75)
         .attr("text-anchor","middle")
         .attr("transform", "rotate(90)")
         .attr("class", "label")
         .text("Relative Preference (Difference/Sum)");

   var truncateText = function (str){
     if(str.length < 25){
       return str;
     } else {
       return str.slice(0, 25) + " \u2026";
     }
   };

   colourAxisElem.append("text")
         .attr("class", "label")
         .attr("x", 0).attr("y", -25)
         .attr("text-anchor","middle")
         .text("Disproportionately prefer:");
   colourAxisElem.append("text")
         .attr("class", "label")
         .attr("x", 0).attr("y", -15)
         .attr("text-anchor","middle")
         .text(truncateText(yname));
   colourAxisElem.append("text")
         .attr("class", "label")
         .attr("x", 0).attr("y", 620)
         .attr("text-anchor","middle")
         .text("Disproportionately prefer:");
   colourAxisElem.append("text")
         .attr("class", "label")
         .attr("x", 0).attr("y", 630)
         .attr("text-anchor","middle")
         .text(truncateText(xname));

   var colourScaleGradient =  defs.insert("svg:linearGradient")
       .attr("x1", "0%").attr("y1","0%").attr("x2", "0%").attr("y2","100%")
       .attr("id", "colourScale");
       
   colourScaleGradient.insert("svg:stop")
       .attr("stop-color", colourscale(minRatio))
       .attr("offset", "0%");
   colourScaleGradient.insert("svg:stop")
       .attr("stop-color", colourscale(maxRatio))
       .attr("offset", "100%");

   mapElems.insert("rect")
       .attr("x", 610)
       .attr("y", 0)
       .attr("width", 25)
       .attr("height", 600)
       .attr("fill", "url(#colourScale)");

   var mapMode = false;

   var mapButton = canvas.insert("g")
      .attr("class", "button");
        
   mapButton.insert("rect")
     .attr("x", -45)
     .attr("y", -45)
     .attr("width", 15)
     .attr("height", 15);

   var mapButtonPath = mapButton.insert("path")
     .attr("transform", "translate(-48, -324)")
     .attr("d", MAP_D);

   var mapButtonText = mapButton.insert("text")
     .attr("y", -50+25/2+5)
     .attr("x", -24)
     .attr("class", "label")
     .text("view as map")

   mapButton.on("click", function(){
       var transition = dots.transition()
           .duration(1000)
           .delay(function(d){
             return 25*(mercatorPath.centroid(d.boundary)[1]);
           })

       var axisTransition = graphElems.transition()
            .duration(2000);
       var mapTransition = mapElems.transition()
            .duration(2000);

       var buttonTransition = mapButtonPath.transition()
            .duration(1000);
       mapMode = !mapMode;
       if(mapMode){
         axisTransition.attr("opacity", 0);
         mapTransition.attr("opacity", 1);
         applyMapProperties(transition);
         dots.on("mouseover", onHoverMap);
         mapButtonText.text("view as graph");
         buttonTransition.attr("d", GRAPH_D);
       } else {
         axisTransition.attr("opacity", 1);
         mapTransition.attr("opacity", 0);
         applyGraphProperties(transition);
         dots.on("mouseover", onHoverGraph);
         mapButtonText.text("view as map");
         buttonTransition.attr("d", MAP_D);
       }
     });

  var text = canvas.insert("g")
     .attr("class", "hovertext")
     .attr("opacity", 0);
  var hoverfields = {
     "name": text.insert("text").attr("class", "name"),
     "mp": text.insert("text").attr("class", "mp"),
     "party" : text.insert("text").attr("class", "party"),
     "population": text.insert("text").attr("class", "population"),
     "votes": text.insert("text").attr("class", "votes")
  };
}

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function mapToPoint(coordinates){
    return coordinates.map(function(d){return [1, 0]});
}

function mapCoordinates(coordinates){
  var step = 2*Math.PI/(coordinates.length-1);
  var circle = coordinates.map(function(d, i){
     return [Math.cos(i*step), Math.sin(i*step)];
  });
  circle[circle.length-1] = circle[0];
  return circle;
}



function boundToCircle(feature){
  var circle = {};
  circle.type = feature.type;
  circle.id = feature.id;
  circle.properties = feature.properties;
  circle.geometry = {};
  circle.geometry.type = feature.geometry.type;
  if( circle.geometry.type == "Polygon"){
    circle.geometry.coordinates = feature.geometry.coordinates.map(mapCoordinates)
  } else if( circle.geometry.type == "MultiPolygon") {
    var maxPolygonLength = d3.max(feature.geometry.coordinates.map(function(d){
       return d3.max(d.map(function(e){
         return e.length;
       }));
    }));
    circle.geometry.coordinates = feature.geometry.coordinates.map(function(d){
      if(d[0].length == maxPolygonLength){
        return d.map(mapCoordinates);
      } else {
        return d.map(mapToPoint);
      }
    });
  }
  return circle;
}

function setLinks(pet1, pet2){
  var links = d3.select("#linksSection");
  links.attr("style", "");

  var buildLink = function(p){
    links.append("a")
      .attr("href", p.links.self.replace(/\.json$/, ""))
      .text(p.data.attributes.action)
  };
  buildLink(pet1);
  links.append("br");
  buildLink(pet2);
}

function displayError(error){
  d3.select("#errorSection")
    .attr("style", "")
}

function loadAndPlot(x, y){
  d3.queue()
    .defer(d3.json, "https://petition.parliament.uk/petitions/" + x + ".json")
    .defer(d3.json, "https://petition.parliament.uk/petitions/" + y + ".json")
    .defer(d3.csv, "constituencies.csv", function(row){
      return {
        "name": row.name,
        "ons_code":row.ons_code,
        "population": parseInt(row.population),
      }
    })
    .defer(d3.json, "constituency_party_ons.json")
    .defer(d3.json, "topo_wpc.json")
    .await(function(error, pet1, pet2, constituencies, parties, topo){

      if(error){
        displayError(error);
        return
      }

      var boundaries = topojson.feature(topo, topo.objects.wpc);
      var boundMap = _.object(_.map(boundaries.features, function(d){
        return [d.id, d];
      }));
      constituencies.forEach(function(c){
        c.party = parties[c.ons_code].party;
        c.mp = parties[c.ons_code].mp;
        c.boundary = boundMap[c.ons_code];
        if(c.boundary){
          c.circle = boundToCircle(c.boundary);
        }
      }); 
      constituencies = constituencies.filter(function(d){
        return d.boundary;            
      });
      var name1 = get_name(pet1);
      var name2 = get_name(pet2);
      var const1 = get_constituencies_data(pet1);
      var const2 = get_constituencies_data(pet2);
      var combined = to_single_list(constituencies,const1, const2);
      window.console.log(combined)
      setLinks(pet1, pet2);
      plot(combined, name1, name2)
  })
}

function createSelectionUI(){

  var urls = {
    "all": "https://petition.parliament.uk/petitions.json?state=all", 
    "open": "https://petition.parliament.uk/petitions.json?state=open", 
    "closed": "https://petition.parliament.uk/petitions.json?state=closed", 
    "rejected": "https://petition.parliament.uk/petitions.json?state=rejected", 
    "awaiting response": "https://petition.parliament.uk/petitions.json?state=awaiting_response", 
    "with response": "https://petition.parliament.uk/petitions.json?state=with_response", 
    "awaiting debate": "https://petition.parliament.uk/petitions.json?state=awaiting_debate", 
    "debated": "https://petition.parliament.uk/petitions.json?state=debated", 
    "not_debated": "https://petition.parliament.uk/petitions.json?state=not_debated"
  };

  d3.json(urls.all, function(error, data) {
    if(error){
      displayError(error);
      return
    }
    var next = data.links.next;
    var list = data.data;
    var selector = d3.select("#selection").append("div")
      .attr("class", "selector")

    selector.append("h2")
      .text("select two petitions to compare")
    selector.insert("label")
      .attr("for", "petition_type")
      .text("Filter petition type: ")
    var types = selector.insert("select")
      .attr("id", "petition_type");
    var more = selector.insert("button")
      .text("load more petitions");

    types.selectAll("option")
      .data(_.pairs(urls))
      .enter()
      .append("option")
      .attr("value", function(d){return d[1]})
      .text(function(d){return d[0]});

    var form = selector.insert("form");

    var addOptions = function(select, list){
      var sel = select.selectAll(".option").data(list, function(d){
        return d.id;
      });
      sel.enter()
        .append("option")
        .attr("class", "option")
        .attr("value", function(d){
          return d.id;
        })
        .text(function(d){
          return d.attributes.action;
        });
        sel.exit().remove();
      return select;
    };
    form.insert("label")
      .attr("for", "xoptions")
      .text("X Axis");
    form.insert("select")
      .attr("name", "x")
      .attr("id", "xoptions");
    var x = addOptions(d3.select("#xoptions"), list);
    form.insert("br");
    form.insert("label")
      .attr("for", "yoptions")
      .text("Y Axis");
    form.insert("select")
      .attr("name", "y")
      .attr("id", "yoptions");
    var y = addOptions(d3.select("#yoptions"), list);
    more.on("click", function(){
      d3.json(next, function(data){
        next = data.links.next;
        list = list.concat(data.data);
        addOptions(x, list);
        addOptions(y, list);
      });
    })
    types.on("change", function(){
      d3.json(this.value, function(data){
        next = data.links.next;
        list = data.data;
        addOptions(x, list);
        addOptions(y, list);
      });
    })
    form.insert("br");
    form.insert("input")
      .attr("type", "submit")
      .attr("value", "Compare these petitions");
    selector.append("br")
    selector.append("h2").text("Or \u2026")
    selector.append("h2").text(
      "\u2026 chose two petitions by their ID"
    );
    var textForm = selector.append("form");
    textForm.append("label")
      .attr("for", "xid")
      .text("X Axis: ");
    textForm.append("input")
      .attr("name", "x")
      .attr("id", "xid")
      .attr("type", "text");
    textForm.append("br");
    textForm.append("label")
      .attr("for", "yid")
      .text("Y Axis: ");
    textForm.append("input")
      .attr("name", "y")
      .attr("id", "yid")
      .attr("type", "text");
    textForm.append("br")
    textForm.append("input")
      .attr("type", "submit")
      .attr("value", "Compare the petitions with these IDs");
  });
}

window.onload = function(){
  var x = getParameterByName("x");
  var y = getParameterByName("y");
  if( x === null || y === null ){
      createSelectionUI();
  } else {
      loadAndPlot(x, y);
  }
}
